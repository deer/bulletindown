use clap::{Parser, ValueEnum};

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
pub struct Args {
    #[arg(
        help = "The dialect/flavour of BBCode to emit.",
        short,
        long,
        required = true,
        value_enum
    )]
    pub dialect: Dialect,
    #[arg(
        help = "A path to the input Markdown file. Defaults to stdin.",
        short,
        long,
        value_parser
    )]
    pub input: Option<String>,
    #[arg(
        help = "A path to the output BBCode file. Defaults to stdout.",
        short,
        long,
        value_parser
    )]
    pub output: Option<String>,
    #[arg(
        help = "Enable non-CommonMark (GFM) table syntax.",
        short,
        long,
        value_parser
    )]
    pub tables: bool,
    #[arg(
        help = "Enable non-CommonMark (GFM) footnote syntax.",
        short,
        long,
        value_parser
    )]
    pub footnotes: bool,
    #[arg(
        help = "Enable non-CommonMark (GFM) strikethrough syntax.",
        short,
        long,
        value_parser
    )]
    pub strikethrough: bool,
    #[arg(
        help = "Allow non-CommonMark (GFM) tasklist syntax.",
        long,
        value_parser
    )]
    pub tasklists: bool,
    #[arg(help = "Enable “smart punctuation”.", long, value_parser)]
    pub smart_punctuation: bool,
    #[arg(
        help = "Warn when emitting non-UCS-2 characters in XenForo dialect.",
        long_help = "Warn when emitting non-UCS-2 characters (specifically, \
any codepoints U+fffe or larger) in the XenForo dialect. Some XenForo \
implementations will discard(!) these characters, leading to broken output. \
Warnings are printed to stderr.",
        short,
        long,
        value_parser
    )]
    pub encoding_warnings: bool,
}

#[derive(ValueEnum, Clone, Copy, PartialEq, Eq)]
pub enum Dialect {
    Xenforo,
    Proboards,
}
